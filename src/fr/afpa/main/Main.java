package fr.afpa.main;
import java.util.*;

import fr.afpa.action.*;
import fr.afpa.donjon.*;
import fr.afpa.objet.*;
import fr.afpa.personnage.*;
import fr.afpa.salle.*;
import fr.afpa.salle.Salle.Direction;
//c'est un test
/**
 * La Class Main 
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public class Main {
		
	//ATTRIBUTS

	private RandomGenerator randomGenerator = new RandomGenerator(); 
	//Generate randoms elements of the game (monster, item, room..) 
	private static Player player = new Player(new SalleSortie()); //Player, initialized with an Exit Room

	
	//METHODES
	
	
	/**
	 * Initialize the game, fill the room 
	 */
	public static void init()
	{

		int input = 10;	//ScannerInt.readInt(104-4)+1;

		while (input > 0) //Add rooms consecutively
		{
			Salle randomRoom = RandomGenerator.generateRandomSalle(2,2,3);
			Direction southDirection = Direction.S;	
			player.getSalle().addPorte(southDirection, randomRoom);
			player.changeSalle(southDirection);
			input --;
		}
	}
	
	/**
	 * Check if the player is in an ExitRoom
	 * @return true if the player is in an ExitRoom
	 */
	
	public static boolean goodEnding() //If the player is in an ExitRoom
	{
		System.out.println("");
		System.out.println("You won !");
		return true;
	}
	
	/**
	 * Check if the player is dead
	 * @return false if the player is dead
	 */
	public static boolean badEnding() //If the player is dead
	{
		player.die();
		return false;
	}
	
	/**
	 * Test if the player is still in the room
	 */
	public static boolean stillInTheSameRoom (Salle otherRoom)
	{
		return player.getSalle() == otherRoom;
	}
	
	
	//m�thode pour afficher les actions possibles pour le joueur tant qu'il n'a pas quitter la salle
	
	public static void oneTurn()
	{
		Salle currentPlayerRoom = player.getSalle();
		while (stillInTheSameRoom(currentPlayerRoom))
		{
			player.setAllActions();
			player.DescribesEntity();
			int input = ScannerInt.readInt(player.getActionPossible().size());
			player.getActionPossible().get(input).execute(player);
		}
	}
	
	/**
	 * Principal function of the game
	 */
	public static boolean play()
	{

		System.out.println("");
		
		System.out.println("-------------------------------------");
		System.out.println("----> BIENVENUE DANS LE DONJON <----");
		System.out.println("-------------------------------------");
		System.out.println("");
		System.out.println("");
		while (!player.isDead())
		{
			if (player.getSalle().isExit())
			{
				return Main.goodEnding();
			}
			else
			{
				player.setAllActions();	
				player.DescribesEntity();
				player.getSalle().displaySalle();
				
				Main.oneTurn();
			}
			
			
		}
		return Main.badEnding();
	}

	public static void main (String[] args)
	{

		Main.init();
		Main.play();
	}
}

