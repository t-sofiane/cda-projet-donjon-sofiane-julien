package fr.afpa.action;
import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.personnage.*;
import fr.afpa.donjon.*;
import java.util.*;
import java.util.Random;

/**
 * ActionUtiliser permet d'utiliser les objets de la salle
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public class ActionUtiliser implements Action {

	//METHODES

	public boolean isPossible(Player p) {
		
		return p.getSalle().isThereAnObjet();
	}
	

	public void execute(Player p) {
		if (isPossible(p))
		{
		System.out.println("Choisir un objet ");
		int indexSelectedItem = ScannerInt.readInt(p.getSalle().getListObjet().size());
		Objet selectedItem = p.getSalle().getListObjet().get(indexSelectedItem);
		selectedItem.use(p);
		p.getSalle().removeObjet(selectedItem);
		}
		else
		{
			System.out.println("Pas d'bjet disponible dans cette salle !");
		}
		
	

	}
	

	public void describesAction()
	{
		System.out.println(this.toString());
	}
	
	public String toString() {
		return " ActionUtiliser : utiliser un objet";
	}

}
