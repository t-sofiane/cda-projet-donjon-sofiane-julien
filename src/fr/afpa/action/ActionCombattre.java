package fr.afpa.action;


import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.personnage.*;
import fr.afpa.donjon.*;
import java.util.*;

/**
 * ActionCombattre permet au joueur d'attaquer un monstre et ce dernier n'est pas mort va attaquer � son tour le joueur
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public class ActionCombattre implements Action {
	
	static final Scanner scanner = new Scanner(System.in); 

	//METHODES
	
	public boolean isPossible(Player p) {
		
		return p.getSalle().isThereAMonster();
	}
	
	public void execute(Player p) {
		if (isPossible(p))
		{
		 System.out.println(" Veuillez choisir un enemie � attaquer ");
		 int input = ScannerInt.readInt(p.getSalle().getListMonster().size());
		 p.attack(p.getSalle().getListMonster().get(input));
		}
	}	
	
	public void describesAction()
	{
		System.out.println(this.toString());
	}

	public String toString() {
		return " Attaquer un enemie";
	}
	
}
