package fr.afpa.action;

import fr.afpa.personnage.*;

/**
 * L'interface Action
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public interface Action {

	public boolean isPossible(Player p);
	
	public void execute(Player p);
	
	public void describesAction();
	
	public String toString();
}
