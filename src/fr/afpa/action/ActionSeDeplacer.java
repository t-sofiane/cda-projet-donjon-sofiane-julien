package fr.afpa.action;

import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.salle.Salle.Direction;
import fr.afpa.personnage.*;
import fr.afpa.donjon.*;
import java.util.*;
import java.util.Random;

/**
 * ActionSedeplacer
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public class ActionSeDeplacer implements Action {

	//METHODES
	

	public boolean isPossible(Player p) {
		
		return !p.getSalle().isThereAMonster();
	}
	


	public void execute(Player p) {
		
		Iterator <Direction> it = p.getSalle().getPossibleDirection().iterator();
		
		if (isPossible(p))
		{
			System.out.println("Choisir la direction : N=0 | S=1 | E=2 | W=3");
			int input = ScannerInt.readInt(p.getSalle().getPossibleDirection().size());
			while (input > 0)
			{
				it.next();
				input--;
			}
			p.changeSalle(it.next());
		}	
	}
	

	public void describesAction()
	{
		System.out.println(this.toString());
	}

	public String toString() {
		return " ActionSeDeplacer : Changer salle";
	}
	


}

