package fr.afpa.action;

import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.personnage.*;
import fr.afpa.donjon.*;
import java.util.*;
import java.util.Random;

/**
 * ActionSeReposer permet au joueur de r�cup�rer des points de vie et possible que s'il n'y a pas de monstres dans la salle
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public class ActionSeReposer implements Action {

	public boolean isPossible(Player p) {
		
		return !p.getSalle().isThereAMonster();
	}
	
	public void execute(Player p) {
		if (isPossible(p))
		{
		Random randomGenerator = new Random();
		int rest = randomGenerator.nextInt(10);
		p.addLifePoints(rest);
		p.removeAction(this);
		}
	}
	
	public void describesAction()
	{
		System.out.println(this.toString());
	}

	public String toString() {
		return " Se reposer (r�cup�rer des points de vie)";
	}
	
}
