package fr.afpa.action;
import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.personnage.*;
import java.util.*;

/**
 * La classe ActionRegarder permet au joueur de visulaliser les composants de la chambre
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public class ActionRegarder implements Action {
	 
	//METHODES
	
	public boolean isPossible(Player p) {
		return true;
	}
	
	 // R�cup�rer le contenu d'une salle
	
	public static void displayMonster (List <Monster> m)
	{
		int index = 0;
		Iterator <Monster> i = m.iterator();
		{
			while (i.hasNext())
			{
				System.out.println("#  "+index + i.next().toString());
				index++;
			}
		}
	}
	public static void displayObjet (List <Objet> o)
	{
		int index = 0;
		Iterator <Objet> i = o.iterator();
		{
			while (i.hasNext())
			{
				System.out.println("#  "+index + i.next().toString());
				index++;
			}
		}
	}
	public void execute(Player p) {
		
		System.out.println("");
		Salle currentRoom = p.getSalle();
		System.out.println(currentRoom.toString());
		System.out.println(p.toString());
		System.out.println("######## vous �tes dans la salle N� "+currentRoom.getNumSalle()+ " contenant : #########");
		List <Monster> listMontres = currentRoom.getListMonster();
		ActionRegarder.displayMonster(listMontres);
		List <Objet> listObjets = currentRoom.getListObjet();
		ActionRegarder.displayObjet(listObjets);
		System.out.println("############################################################");
		System.out.println("");
		
		}	
	
	public void describesAction()
	{
		System.out.println(this.toString());
	}

	public String toString() {
		return " Description de la salle ";
	}
			
	}
