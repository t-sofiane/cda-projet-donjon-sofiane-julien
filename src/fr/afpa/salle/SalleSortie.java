package fr.afpa.salle;

import fr.afpa.salle.Salle;

/**
 * La salle de sortie de donjon
 * @author Sofiane TAYEB & Julien DEHORTER
 */
public class SalleSortie extends Salle {
	
	public boolean isExit() {
		return true;
	}

}
