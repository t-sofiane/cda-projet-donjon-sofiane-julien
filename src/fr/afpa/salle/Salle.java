package fr.afpa.salle;

import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.personnage.*;
import fr.afpa.donjon.*;
import java.util.*;

/**
 * La classe Salle 
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */
public class Salle {
	
	//ATTRIBUTS
	
	private List <Monster> listMonstre;
	private List <Objet> listObjet;
	private Map <Direction,Salle> portes;
	private static int numSalle;
	public static int nombreSalle = 0;
	
	//CONSTRUCTEUR
	
	public Salle()
	{
		this.listMonstre = new ArrayList <Monster>();
		this.listObjet = new ArrayList <Objet>();
		this.portes = new HashMap <Direction,Salle>();
		this.numSalle = nombreSalle;
		Salle.numSalle++;
	}
	
	//METHODES
	
	 //Return the Salle's number
	
	public int getNumSalle()
	{
		return this.numSalle;
	}
	
	 // V�rifier si c'est une sortie

	public boolean isExit()
	{
		return false;
	}

	// afficher tout les monstres dans une salle
	
	public List<Monster> getListMonster() {
		return listMonstre;
	}


	//Ajouter un monstre dans une salle
	
	public void addMonster (Monster m) {
		this.listMonstre.add(m);
		m.setSalle(this);
	}
	
	//Supprimer un monstre d'une salle

	public void removeMonster (Monster m) {
		this.listMonstre.remove(m);
	}

	//Return les objets d'une salle
	
	public List<Objet> getListObjet() {
		return listObjet;
	}

	//Ajouter un objet dans une salle
	
	public void addObjet(Objet o) {
		this.listObjet.add(o);
	}
	
	//Supprimer un objet
	
	public void removeObjet(Objet o) {
		this.listObjet.remove(o);
	}
	//Toute les directions possible d'une salle

	public enum Direction {N,S,E,W;
		

		// the opposite direction of a direction

		public Direction getOppositeDirection ()
		{
			if (this == Direction.E)
				return Direction.W;
			else if (this == Direction.W)
				return Direction.E;
			else if (this == Direction.N)
				return Direction.S;
			else
				return Direction.N;
			
		}
		
		public void DescribesDirection()
		{
			System.out.println(this.name());
		}
		
	}

	//R�cup�r� les sorties d'une salle
	
	public Map<Direction, Salle> getPortes() {
		return portes;
	}

	//Ajouter une porte 
	
	public void addPorte(Direction d, Salle s) {
		this.portes.put(d,s);
		s.portes.put(d.getOppositeDirection(),this);
	}
	
	//Afficher les salles voisines
	
	public Salle changeSalle(Direction direction) throws WrongDirectionException
	{
		if (!this.getPossibleDirection().contains(direction))	
		{
			throw new WrongDirectionException ("cette direction n'est pas valide");
		}
		else
		{
			return this.portes.get(direction);
		}
	}
	
	//Les directions possible d'une salle
	
	public Set<Direction> getPossibleDirection()
	{
		return this.portes.keySet();	
	}
	
	//Verifier s'il existe un monstre dans une salle
	
	public boolean isThereAMonster()
	{
		return (!this.listMonstre.isEmpty());
	}
	
	//Verifier s'il existe un objet dans une salle

	public boolean isThereAnObjet()
	{
		return (!this.listObjet.isEmpty());
	}
	
	public String toString() {
		return "Salle "+ numSalle;
	}	
	
	// Afficher la discription d'une salle
	
	public void displaySalle()
	{
		System.out.println(this.toString());
		System.out.println("\nLes Monstres :");
		int index = 0;
		
		for (Monster m : this.getListMonster())
		{
			System.out.print(index + " ");
			m.DescribesEntity();
			index ++;
		}
		index = 0;
		
		System.out.println("\nLes Objets :");
		
		for (Objet obj : this.getListObjet())
		{
			System.out.print(index + " " );
			obj.describesObjet();
			index ++;
		}
		index = 0;
		
		System.out.println("\nLes Directions :");
		
		for (Direction d : this.getPossibleDirection())
		{
			System.out.print(index + " " );
			d.DescribesDirection();
			index ++;
		}
		
		System.out.println("\n");
	}
}
