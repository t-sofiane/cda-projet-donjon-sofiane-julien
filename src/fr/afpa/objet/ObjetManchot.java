package fr.afpa.objet;

import fr.afpa.objet.*;
import fr.afpa.personnage.*;
import fr.afpa.salle.*;
import java.util.*;


/**
 * Le Bandit Manchot produit un objet al�atoirement, utilisable d�s son obtention par le joueur contre des pi�ces d'or
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public class ObjetManchot implements Objet {
	
	//ATTRIBUTS
	
	private List<Objet> listObjets;
	
	ObjetOr price;
	
	//CONSTRUCTEURS
	
	public ObjetManchot (int prix){
		this.price = new ObjetOr (prix);
		this.listObjets = new ArrayList<Objet>();
		this.listObjets.add(new ObjetPotionSoin());
		this.listObjets.add(new ObjetPotionForce());
	}
	
	public ObjetManchot (){
		this.price = new ObjetOr (100);
		this.listObjets = new ArrayList<Objet>();
		this.listObjets.add(new ObjetPotionSoin());
		this.listObjets.add(new ObjetPotionForce());
	}

	//METHODES
	
	/**
	 * Use one armed bandit
	 */
	public void use(Personnage pers){
		if (pers.getObjetOr().getQuantity() >= this.price.getQuantity()){
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(this.listObjets.size());
			Objet p = this.listObjets.get(randomInt);
			pers.subtractObjetOr(this.price);
			p.use(pers);
			System.out.println("le joueur a perdu " + this.price);
			pers.getSalle().removeObjet(this);
		}	
		else
			{
			pers.getSalle().removeObjet(this);
			}
	}

	public String toString() {
		return " bandit manchot : price=" + price;
	}
	
	public void describesObjet()
	{
		System.out.println(this.toString());
	}
	
	
}