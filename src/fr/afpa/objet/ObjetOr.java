package fr.afpa.objet;

import fr.afpa.personnage.*;
import fr.afpa.objet.*;

/**
 * Simulates in-game gold coins
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public class ObjetOr implements Objet{
	
	//ATTRIBUTS
	
	int gold;
	
	//CONSTRUCTEURS
	
	
	public ObjetOr (int goldQuantity){
		this.gold = goldQuantity;
	}
	
	public ObjetOr (){
		this.gold = 100;
	}

	//METHODES
	
	
	public int getQuantity(){
		return this.gold;
	}
	
	
	public void addQuantity(ObjetOr or){
		this.gold += or.getQuantity();
	}
	
	public void subtractQuantity(ObjetOr or){
		this.gold -= or.getQuantity();
	}

	
	
	public void use(Personnage pers){
		pers.addObjetOr(this);
		System.out.println("You picked up " + this.toString());
		pers.getSalle().removeObjet(this); // object has been used !		
	}
	

	public String toString() {
		return " " + this.gold + " pi�ces d'or";
	}
	
	public void describesObjet()
	{
		System.out.println(this.toString());
	}
	
	
}
