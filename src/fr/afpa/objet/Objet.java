package fr.afpa.objet;

import fr.afpa.personnage.*;

/**
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public interface Objet {
	
	
	public void use (Personnage pers);
	
	
	public String toString();
	
	
	public void describesObjet();

}
