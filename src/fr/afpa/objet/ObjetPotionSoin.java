package fr.afpa.objet;

import fr.afpa.personnage.*;
import fr.afpa.objet.*;

/**
 * R�cup�rer de points de vie par le joueur
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public class ObjetPotionSoin extends Potion {

	//CONSTRUCTEURS
	
	public ObjetPotionSoin (int lifePoints){
		super(lifePoints);
	}

	public ObjetPotionSoin (){
		super();
	}
	
	//METHODES
	
	/**
	 * Use lifepotion 
	 */
	public void use(Personnage dc){
		dc.addLifePoints(this.points);
		dc.getSalle().removeObjet(this);
	}
	
	public String toString() {
		return " Potion vie : " + this.points + " points de vie";
	}
	
	public void describesObjet()
	{
		System.out.println(this.toString());
	}
}
