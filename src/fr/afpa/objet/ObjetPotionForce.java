package fr.afpa.objet;

import fr.afpa.personnage.*;
import fr.afpa.objet.*;

/**
 * Type of potion
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public class ObjetPotionForce extends Potion {

	//CONSTRUCTEURS
	
	public ObjetPotionForce (int StrengthPoints){
		super(StrengthPoints);
	}
	
	public ObjetPotionForce (){
		super();
	}

	//METHODES 
	
	/**
	 * Utiliser les points de force 
	 */
	public void use(Personnage pers){
		pers.addStrengthPoints(this.points);
		pers.getSalle().removeObjet(this);
	}

	public String toString() {
		return " potion force : " + this.points + " points de force";
	}
	public void describesObjet()
	{
		System.out.println(this.toString());
	}
}