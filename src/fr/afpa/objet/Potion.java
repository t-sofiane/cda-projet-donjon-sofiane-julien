package fr.afpa.objet;

import fr.afpa.personnage.*;
import fr.afpa.objet.*;

/**
 * Type of potion
 * @author Sofiane TAYEB & Julien DEHORTER
 *
 */

public  abstract class Potion implements Objet {
	
		//ATTRIBUTS
		protected int points;
		
		//CONSTRUCTEUR
		
		public Potion (int Points){
			this.points = Points;
		}
		
		public Potion (){
			this.points = 10;
		}	
		
		//METHODES
		
		public abstract void use(Personnage dc);

		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Potion other = (Potion) obj;
			if (points != other.points)
				return false;
			return true;
		}
		
		public void describesObjet()
		{
			System.out.println(this.toString());
		}
		
}
