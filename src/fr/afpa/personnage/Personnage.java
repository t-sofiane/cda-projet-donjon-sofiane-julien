package fr.afpa.personnage;

import fr.afpa.objet.*;
import fr.afpa.salle.*;


import fr.afpa.objet.*;
import fr.afpa.salle.*;
 


import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.config.ReadPropertyFile;

 


import fr.afpa.donjon.*;

/**
 * la Superclass de tout les personnage de jeux 
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public abstract class Personnage {
	
	//ATTRIBUTS
	
	protected int pointVie;
	protected int pointForce;
	protected ObjetOr or;
	protected Salle salle;
	
	//CONTRUCTEURS
	
	public Personnage (int lifePoints, int strengthPoints, Salle Salle, int or)
	{
		this.pointVie = lifePoints;
		this.pointForce = strengthPoints;
		this.or = new ObjetOr(or);
		this.salle = Salle;
	}
	
	public Personnage (Salle Salle)
	{

		this.pointVie = 30;
		this.pointForce = 10;
		this.pointVie = ReadPropertyFile.PropertyLoad ("HP");
		this.pointForce = ReadPropertyFile.PropertyLoad ("Strength");
 

		this.pointVie = 30;
		this.pointForce = 10;
 
		this.pointVie = ReadPropertyFile.PropertyLoad ("HP");
		this.pointForce = ReadPropertyFile.PropertyLoad ("Strength");


		this.or = new ObjetOr(100);	
		this.salle = Salle;
	}
	
	//METHODES

	//R�cup�rer les points de vie d'un personnage
	
	public int getPointVie() {
		return pointVie;
	}

	//Ajouter un point de vie pour un personnage
	
	public void addLifePoints(int lifePoints) {
		this.pointVie += lifePoints;
		String Entityname = this.toString().substring(0, this.toString().indexOf("[")-1);
		System.out.println(Entityname + " gagner " + lifePoints + " points de vie");
	}
	
	//Diminuer des points de vie d'un personnage
	
	public void substractLifePoints(int lifePoints) {
		this.pointVie -= lifePoints;
		String Entityname = this.toString().substring(0, this.toString().indexOf("[")-1);
		System.out.println(Entityname + " perdre " + lifePoints + " points de vie");
	}

	//R�cup�rer les points de force d'un personnage
	
	public int getPointForce() {
		return pointForce;
	}

	//Ajouter des points de force pour personnage

	public void addStrengthPoints(int strengthPoints) {
		this.pointForce += strengthPoints;
		String Entityname = this.toString().substring(0, this.toString().indexOf("[") -1);
		System.out.println(Entityname + " gagner " + strengthPoints + " points de force");
	}

	//R�cup�rer la salle actuelle d'un personnage
	
	public Salle getSalle() {
		return salle;
	}

	//Changer la salle actuelle d'un personnage

	public void setSalle(Salle Salle) {
		this.salle = Salle;
	}
	
	//R�cup�rer le nombre de pi�ces d'or d'un personnage
	
	public ObjetOr getObjetOr() {
		return this.or;
	}

	//Augmenter le nombre de pi�ces d'or d'un personnage

	public void addObjetOr(ObjetOr or) {
		this.or.addQuantity(or);
	} 
	
	//Augmenter le nombre de pi�ces d'or d'un personnage

	public void subtractObjetOr(ObjetOr or) {
		this.or.subtractQuantity(or);
	} 
	
	//V�rifier si un personnage est mort
	
	public boolean isDead()
	{
		return this.pointVie <= 0;
	}
	
	
	/**
	 * Applies some effects if the character is dead
	 */
	public abstract void die(); 
	
	//Attaquer un personnage, si le personnage attaqu� n'est pas mort il va attaquer lui aussi
	
	public void attack (Personnage pers){
		pers.substractLifePoints(this.pointForce);
		if (pers.isDead())
		{
			pers.die();
		}
		else
		{
			this.substractLifePoints(pers.getPointForce());
		}
	}
	
	/**
	 * Describe an entity
	 */
	public void DescribesEntity()
	{
		System.out.println(this.toString());
	}
}
