package fr.afpa.personnage;

import fr.afpa.objet.*;
import fr.afpa.salle.*;
import fr.afpa.salle.Salle.Direction;
import fr.afpa.donjon.*;
import fr.afpa.action.*;
import java.util.*;

/**
 * Player class
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public class Player extends Personnage {
	
	//ATTRIBUTS
	
	protected List<Action> actionPossible;

	//CONSTRUCTEURS
	
	public Player(int pointVie, int pointForce, Salle salle, int or) {
		super(pointVie, pointForce, salle, or);
		this.actionPossible = new ArrayList<Action>();
		

	}
	
	public Player(Salle salle) {
		super(salle);
		this.actionPossible = new ArrayList<Action>();
	}
	
	//METHODES
	
	//Les Actions de player
	
	public String actionsPossiblePlayer()
	{
		int index = 0;
		String actionsPossibles = "********** LES ACTIONS POSSIBLES ************\n";
		
		for (Action a : this.actionPossible)
		{
			actionsPossibles += index + a.toString()+"\n";
			index ++;
		}
		actionsPossibles += "************************************************\n\n\n";
		return actionsPossibles;
	}

	public String toString() {
		return "*** VOS CARACTERISTIQUES :  [VIES="+pointVie+", FORCE="+pointForce+", OR="+or+"] *** \n"+ "\n" + this.actionsPossiblePlayer();
	}


	// Si le joueur est mort
	
	public void die() {
		System.out.println("You died! GAME OVER.");

	}
	
	//changer la salle
	
	public void changeSalle (Direction direction)
	{
		Salle nextRoom = this.salle.changeSalle(direction);
		this.salle = nextRoom;
	}
	
	// Ajouter une action pour le joueur
	
	public void addAction (Action action)
	{
		if (action.isPossible(this))
		{
			this.actionPossible.add(action);
		}
		
	}
	
    // Supprimer une action pour le joueur
	
	public void removeAction (Action action)
	{
		actionPossible.remove(action);
	}
	
	// set toutes les actions possibles pour le joueur
	
	public void setAllActions()
	{
		this.actionPossible.clear();
		List<Action> allActions = new ArrayList<Action>();
		allActions.add(new ActionCombattre());
		allActions.add(new ActionRegarder());
		allActions.add(new ActionSeReposer());
		allActions.add(new ActionUtiliser());
		allActions.add(new ActionSeDeplacer());
		Iterator <Action> it = allActions.iterator();
		while (it.hasNext())
		{
			Action action = it.next();
			this.addAction(action);
		}
		
	}
	
	// Liste de toutes les actions possibles
	
	public List<Action> getActionPossible()
	{
		return this.actionPossible;
	}
	
}
