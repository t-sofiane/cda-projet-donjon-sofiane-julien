package fr.afpa.personnage;

import fr.afpa.salle.*;
import fr.afpa.objet.*;
import java.util.Random;

/**
 * Class Monster 
 * @author Sofiane TAYEB & Julien DEHORTER
 */

public class Monster extends Personnage {
	
	//CONSTRUCTEURS

	
	public Monster(int lifePoints, int strengthPoints, Salle salle, int or) {
		super(lifePoints, strengthPoints, salle, or);
	}

	public Monster(Salle salle) {
		super(salle);
	}
	
	//METHODES
	
	public String toString() {
		return " Monster [VIE=" + pointVie + ", FORCE=" + pointForce + ", OR=" + or+"]";
	}

	//Quant le monstre meurt, il d�sparait et il fait tomber son or dans la salle
	
	public void die() {
		System.out.println("Le monstre est mort et a laiss� " + this.getObjetOr().toString());
		Salle MonsterRoom = (Salle)this.salle;
		MonsterRoom.addObjet(this.getObjetOr());
		MonsterRoom.removeMonster(this);
	}

}