package fr.afpa.donjon;

import java.util.*;
import fr.afpa.objet.*;
import fr.afpa.personnage.*;
import fr.afpa.salle.*;
import fr.afpa.salle.Salle.Direction;
/**
 * Creates random Dungeon's objects, in order to generate random game 
 * @author Sofiane TAYEB & Julien DEHORTER
 */


public class RandomGenerator {
	
	//METHODES
	
	// Generate a random monster
	
	public static Monster generateRandomMonster(int maxVie, int maxForce, Salle salle, int maxOr)
	{
		Random randomGenerator = new Random();
		int randomLife = randomGenerator.nextInt(maxVie-1)+1;
		int randomStrength = randomGenerator.nextInt(maxForce-1)+1;
		int randomGold = randomGenerator.nextInt(maxOr-1)+1;
		return new Monster (randomLife,randomStrength,salle,randomGold);
	}
	
	//Generate default monster 
	public static Monster generateRandomMonster(Salle salle)
	{
		return generateRandomMonster (20,3,salle,20);
	}
	
	
	// Generate a random Objet
	
	public static Objet generateRandomObjet(int maxVie, int maxForce, int maxOr)
	{
		Random randomGenerator = new Random();
		int randomLife = randomGenerator.nextInt(maxVie-1)+1;
		int randomStrength = randomGenerator.nextInt(maxForce-1)+1;
		int randomGold = randomGenerator.nextInt(maxOr-1)+1;
		
		List <Objet> allObjets = new ArrayList<Objet>();
		Objet lifePotion = new ObjetPotionSoin (randomLife);
		Objet strengthPotion = new ObjetPotionForce (randomStrength);
		Objet oneArmedBandit = new ObjetManchot (randomGold);
		
		allObjets.add(lifePotion);
		allObjets.add(oneArmedBandit);
		allObjets.add(strengthPotion);
		int randomObjet = randomGenerator.nextInt(allObjets.size());
		return allObjets.get(randomObjet);
	}
	
	//Generate default objet
	
	public static Objet generateRandomObjet()
	{
		return generateRandomObjet (10,3,50);
	}
	
	//Generer des salle sans portes
	public static Salle generateRandomSalle (int nombreMonstres, int nombreObjets)
	{
		
		return generateRandomSalle (nombreMonstres, nombreObjets, 0);
	}
	
	//Generer des salle avec des portes
	
	public static Salle generateRandomSalle (int nombreMonstres, int nombreObjets, int nombrePortes)
	{
		
		List <Direction> allDirections = new ArrayList <Direction>();
		allDirections.add(Direction.N);
		allDirections.add(Direction.S);
		allDirections.add(Direction.E);
		allDirections.add(Direction.W);
		Iterator <Direction> it = allDirections.iterator();
		Salle randomSalle = new Salle();
		while (nombreMonstres > 0)
		{
			randomSalle.addMonster(RandomGenerator.generateRandomMonster(randomSalle));			
			nombreMonstres --;
		}
		
		while (nombreObjets > 0)
		{
			randomSalle.addObjet(RandomGenerator.generateRandomObjet());			
			nombreObjets --;
		}
		
		while (nombrePortes > 0 )
		{
			randomSalle.addPorte(it.next(), generateRandomSalle(nombreMonstres,nombreObjets));	
			nombrePortes --;
		}
		
		
		return randomSalle;
	}
	public void displayDonjon() {
		
	}

}
