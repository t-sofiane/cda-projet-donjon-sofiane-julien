package fr.afpa.donjon;

import java.util.*;
import java.util.regex.*;



/**
 * Generate an input for integer
 * @author jc
 */

public class ScannerInt {
	 static final Scanner scanner = new Scanner(System.in);

	/**
	 * Read an integer from 0 (included) to n (excluded) from standard input 
	 */
	public static int readInt(int n) {
		int input = -1;
		while (input < 0 || input >= n) {
			System.out.print("your choice (0-" + (n - 1) + ") ? ");
			try {
				input = scanner.nextInt();
			} catch (InputMismatchException	 e){
				// consume the input (that is not an integer)
				scanner.skip(".*");
			}
		} 
		return input;
	}
}
